# OpenML dataset: rsctc2010_2

https://www.openml.org/d/1078

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Data from the RSCTC 2010 Discovery Challenge. Example datasets for 6 different problems of DNA microarray data analysis and classification. All datasets contain gene expression data characterized by values of 20,000 - 65,000 attributes. Samples are assigned to several (2-10) classes. All attributes are numeric and represent measurements from DNA microarrays. 

Source: http://tunedit.org/challenge/RSCTC-2010-A

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1078) of an [OpenML dataset](https://www.openml.org/d/1078). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1078/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1078/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1078/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

